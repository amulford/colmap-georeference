# Goal: Produce geo-referenced & orientated dense models with Colmap

`setup.sh` shows required dependencies to run this (excluding colmap which must be available on CLI: [How to install Colmap](https://colmap.github.io/install.html))

Current workflow (colma):

1) `process.sh` details the steps we are taking to generate a dense, georeferenced model with colmap
2) `coords.sh` is called from `process.sh` and converts image GPS to ECEF(m) cartesian coordinates

Below is a link to the sample village data we are using:

[Sample village data](https://trikdependencies.blob.core.windows.net/data/village.zip)

And here is a link to the poor quality dense reconstruction that this workflow creates:

[Output data from above workflow](https://trikdependencies.blob.core.windows.net/data/colmap-village-geo.zip)


IMAGES=$2
OUTPUT=$2

echo "Creating project from $IMAGES directory"

DATABASE=$OUTPUT/database.db
SPARSE=$OUTPUT/sparse
SPARSE_GEO=$OUTPUT/sparse-geo
DENSE=$OUTPUT/dense

mkdir -p $SPARSE
mkdir -p $SPARSE_GEO && mkdir -p $SPARSE_GEO/0
mkdir -p $DENSE

colmap feature_extractor \
	--database_path $DATABASE \
	--image_path $IMAGES

colmap exhaustive_matcher \
	--database_path $DATABASE

colmap mapper \
	--database_path $DATABASE \
	--image_path $IMAGES \
	--output_path $SPARSE

# Generate ECEF coords
COORDS="coords.txt"
ECEF="ecef.txt"

bash coords.sh $IMAGES $COORDS $ECEF

NUM_ECEF_LINES=$(wc -l < $ECEF)

echo "Found $NUM_ECEF_LINES coordinates in ecef.txt"

if [ $NUM_ECEF_LINES -gt 2 ]; then
	colmap model_aligner --input_path $SPARSE/0 --output_path $SPARSE_GEO/0 --ref_images_path $ECEF --robust_alignment 1 --robust_alignment_max_error 1000
	SPARSE=$SPARSE_GEO
	echo "Continung with georeferenced sparse model... $SPARSE"
fi

colmap image_undistorter \
    --image_path $IMAGES \
    --input_path $SPARSE/0 \
    --output_path $DENSE \
    --output_type COLMAP \
    --max_image_size 2000

colmap patch_match_stereo \
    --workspace_path $DENSE \
    --workspace_format COLMAP \
    --PatchMatchStereo.geom_consistency true

colmap stereo_fusion \
    --workspace_path $DENSE \
    --workspace_format COLMAP \
    --input_type geometric \
    --output_path $DENSE/fused.ply

colmap poisson_mesher \
    --input_path $DENSE/fused.ply \
    --output_path $DENSE/meshed-poisson.ply

colmap delaunay_mesher \
    --input_path $DENSE \
    --output_path $DENSE/meshed-delaunay.ply









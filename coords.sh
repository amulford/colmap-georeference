#!/bin/bash

# Extract EXIF GPS data from images and create CSV
exiftool -p '$filename $gpslatitude $gpslongitude $gpsaltitude' -n -csv $1 > $2

# Create ECEF txt
rm -f $3
while IFS=' ' read -r filename lat lon alt
do
	if [[ -z $lat || -z $lon || -z $alt ]]; then
		continue
	fi 
	COORDS=$(echo $lon $lat $alt | cs2cs +proj=latlong +ellps=WGS84 +datum=WGS84 +to +proj=geocent +ellps=WGS84 +datum=WGS84)
	echo "$filename $COORDS" >> $3	
done < "$2"
sed -i.bak $'s/\t/ /' $3
